FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8761
ADD ./target/ramosvji-discoveryserver-0.0.1-SNAPSHOT.jar /ramosvji-discoveryserver.jar
ENTRYPOINT ["java","-jar","/ramosvji-discoveryserver.jar","com.ramosvji.discovery.RamosvjiDiscoveryserverApplication"]