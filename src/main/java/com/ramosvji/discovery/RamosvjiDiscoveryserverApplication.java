package com.ramosvji.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class RamosvjiDiscoveryserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(RamosvjiDiscoveryserverApplication.class, args);
	}

}
